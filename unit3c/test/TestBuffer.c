#include "unity.h"
#include "Buffer.h"
#include "MK20DX256.h"

#define DIMENSION_OF(a) sizeof(a)/sizeof(a[0])
#define ACTUAL_BUFFER_SIZE (BUFFER_SIZE - 1)
#define GEN_BUFFER_SIZE 24

void tearDown(void) 
{
  uint16_t fetched;
  while ( !Buffer_Get(&fetched) );
}

//  Testing both Buffer Put and Get otherwise tests will have to access
//    internal state, which may be messy.
void test_Buffer_Put_and_Get_should_WorkTogetherToInsertAndExtractAValue(void)
{
  const uint16_t data[] = { 0, 1, 0x5A, 0x7FFF, 0xFFFF };
  uint16_t fetched;

  for ( int i = 0; i < DIMENSION_OF(data); i++) {
    TEST_ASSERT_EQUAL(0, Buffer_Put( data[i] ));
    TEST_ASSERT_EQUAL(0, Buffer_Get( &fetched ));
    TEST_ASSERT_EQUAL_HEX16(data[i], fetched);
  }
}

void test_Buffer_Get_should_ReturnErrorIfCalledWhenEmpty(void)
{
  uint16_t fetched;

  TEST_ASSERT_NOT_EQUAL(0, Buffer_Get(&fetched));
}

void test_Buffer_Put_should_ReturnErrorIfCalledWhenFull(void)
{
  uint16_t fetched;

  int Expected[ ACTUAL_BUFFER_SIZE ];
  int Actual[ ACTUAL_BUFFER_SIZE ];

  //  Fix was increasing the ring buffer by size of 1.
  for (int i = 0; i < ACTUAL_BUFFER_SIZE; i++) {
    Expected[ i ] = 0;
    Actual[ i ] = Buffer_Put(i);
  }

  TEST_ASSERT_EQUAL_INT_ARRAY(Expected, Actual, ACTUAL_BUFFER_SIZE );
  TEST_ASSERT_NOT_EQUAL_MESSAGE(0, Buffer_Put(0xFE), "Is Buffer full?");
}

void test_Buffer_Get_should_ReturnValuesInInsertionOrder(void)
{
  uint16_t fetched;
  int Expected[ GEN_BUFFER_SIZE ];
  int Actual[ GEN_BUFFER_SIZE ];

  for (int i = 0; i < GEN_BUFFER_SIZE; i++) {
    Expected[ i ] = i;
    TEST_ASSERT_EQUAL(0, Buffer_Put(i));
  }

  for (int i = 0; i < GEN_BUFFER_SIZE; i++) {
    TEST_ASSERT_EQUAL(0, Buffer_Get(&fetched));
    Actual[ i ] = fetched;
  }

  TEST_ASSERT_EQUAL_INT_ARRAY(Expected, Actual, GEN_BUFFER_SIZE );
}

void test_Buffer_Put_and_Get_should_WorkWithInterleaved(void)
{
  uint16_t fetched;
  TEST_ASSERT_EQUAL(0, Buffer_Put(0xBEEF));
  TEST_ASSERT_EQUAL(0, Buffer_Put(0xDEAD));

  TEST_ASSERT_EQUAL(0, Buffer_Get(&fetched));
  TEST_ASSERT_EQUAL(0xBEEF, fetched);

  TEST_ASSERT_EQUAL(0, Buffer_Put(0xBEAD));

  TEST_ASSERT_EQUAL(0, Buffer_Get(&fetched));
  TEST_ASSERT_EQUAL(0xDEAD, fetched);

  TEST_ASSERT_EQUAL(0, Buffer_Put(0xBEEF));

  TEST_ASSERT_EQUAL(0, Buffer_Get(&fetched));
  TEST_ASSERT_EQUAL(0xBEAD, fetched);

  TEST_ASSERT_EQUAL(0, Buffer_Get(&fetched));
  TEST_ASSERT_EQUAL(0xBEEF, fetched);
}

void test_Buffer_Get_should_ReturnErrorWhenReachingEmptyBuffer(void)
{
  uint16_t fetched;

  for (int i = 0; i < GEN_BUFFER_SIZE; i++) {
    TEST_ASSERT_EQUAL(0, Buffer_Put(i));
  }

  for (int i = 0; i < GEN_BUFFER_SIZE; i++) {
    TEST_ASSERT_EQUAL(0, Buffer_Get(&fetched));
  }

  TEST_ASSERT_NOT_EQUAL(0, Buffer_Get(&fetched));
}

void test_Buffer_Put_should_ReturnErrorWhenReachingFullBuffer(void)
{
  uint16_t fetched;

  for (int i = 0; i < ACTUAL_BUFFER_SIZE; i++) {
    TEST_ASSERT_EQUAL(0, Buffer_Put(i));
  }

  TEST_ASSERT_NOT_EQUAL(0, Buffer_Put(0xBEAB));
}

void test_Buffer_IsFull_should_ReturnTrue_given_BufferIsFull(void)
{
  for (int i = 0; i < ACTUAL_BUFFER_SIZE; i++) {
    TEST_ASSERT_EQUAL(0, Buffer_Put(i));
  }

  TEST_ASSERT_TRUE(Buffer_IsFull());
}

void test_Buffer_IsFull_should_ReturnFalse_given_BufferIsEmpty(void)
{
  TEST_ASSERT_FALSE(Buffer_IsFull());
}

void test_Buffer_IsEmpty_should_ReturnFalse_given_BufferIsFull(void)
{
  for (int i = 0; i < ACTUAL_BUFFER_SIZE; i++) {
    TEST_ASSERT_EQUAL(0, Buffer_Put(i));
    TEST_ASSERT_FALSE(Buffer_IsEmpty());
  }

  TEST_ASSERT_FALSE(Buffer_IsEmpty());
}

void test_Buffer_IsEmpty_should_ReturnTrue_given_BufferIsEmpty(void)
{
  TEST_ASSERT_TRUE(Buffer_IsEmpty());
}

void test_Buffer_Clear_should_EmptyBuffer(void)
{
  uint16_t fetched;
  TEST_ASSERT_EQUAL(0, Buffer_Put(0xBEEF)); 
  TEST_ASSERT_EQUAL(0, Buffer_Put(0xBEEF)); 

  TEST_ASSERT_EQUAL(0, Buffer_Clear());
  
  TEST_ASSERT_NOT_EQUAL(0, Buffer_Get(&fetched));
  TEST_ASSERT_TRUE(Buffer_IsEmpty());
}

void test_Buffer_Clear_should_EmptyBuffer_given_FullBuffer(void)
{
  uint16_t fetched;

  for (int i = 0; i < ACTUAL_BUFFER_SIZE; i++) {
    TEST_ASSERT_EQUAL(0, Buffer_Put(0xBEEF)); 
  }  

  TEST_ASSERT_EQUAL(0, Buffer_Clear());
  
  TEST_ASSERT_TRUE(Buffer_Get(&fetched));
  TEST_ASSERT_TRUE(Buffer_IsEmpty());
}

void test_Buffer_Clear_should_EmptyBuffer_given_EmptyBuffer(void)
{
  uint16_t fetched;
  TEST_ASSERT_TRUE(Buffer_Get(&fetched));
  TEST_ASSERT_TRUE(Buffer_IsEmpty());

  TEST_ASSERT_EQUAL(0, Buffer_Clear());
  
  TEST_ASSERT_TRUE(Buffer_Get(&fetched));
  TEST_ASSERT_TRUE(Buffer_IsEmpty());
}

int main(void)
{
  UNITY_BEGIN();
  //  Basic case
  RUN_TEST(test_Buffer_Put_and_Get_should_WorkTogetherToInsertAndExtractAValue);
  RUN_TEST(test_Buffer_Get_should_ReturnValuesInInsertionOrder);
  RUN_TEST(test_Buffer_Put_and_Get_should_WorkWithInterleaved);
  RUN_TEST(test_Buffer_Get_should_ReturnErrorWhenReachingEmptyBuffer);
  RUN_TEST(test_Buffer_Put_should_ReturnErrorWhenReachingFullBuffer);

  //  Error cases
  RUN_TEST(test_Buffer_Get_should_ReturnErrorIfCalledWhenEmpty);
  RUN_TEST(test_Buffer_Put_should_ReturnErrorIfCalledWhenFull);

  //  Buffer state functions
  RUN_TEST(test_Buffer_IsFull_should_ReturnTrue_given_BufferIsFull);
  RUN_TEST(test_Buffer_IsFull_should_ReturnFalse_given_BufferIsEmpty);
  RUN_TEST(test_Buffer_IsEmpty_should_ReturnFalse_given_BufferIsFull);
  RUN_TEST(test_Buffer_IsEmpty_should_ReturnTrue_given_BufferIsEmpty);

  //  Clear buffer 
  RUN_TEST(test_Buffer_Clear_should_EmptyBuffer);
  RUN_TEST(test_Buffer_Clear_should_EmptyBuffer_given_FullBuffer);
  RUN_TEST(test_Buffer_Clear_should_EmptyBuffer_given_EmptyBuffer);

  return UNITY_END();
}
