#include "Buffer.h"
#include "MK20DX256.h"

uint16_t Buffer[BUFFER_SIZE];
uint16_t Buffer_Read = 0, Buffer_Write = 0;;

int Buffer_Put(uint16_t Val) 
{
  uint16_t NextWrite = ( Buffer_Write + 1 ) % BUFFER_SIZE;
  if ( NextWrite == Buffer_Read ) {
    return BUFFER_ERROR_FULL;
  }
  Buffer[Buffer_Write] = Val;  
  Buffer_Write = NextWrite; 
  return 0;
}

int Buffer_Get(uint16_t* Val) 
{
  if ( Buffer_Read == Buffer_Write ) {
    return BUFFER_ERROR_EMPTY;
  }  

  *Val = Buffer[Buffer_Read];  
  Buffer_Read = ( Buffer_Read + 1 ) % BUFFER_SIZE; 
  return 0;
}

bool Buffer_IsFull() 
{
  bool result = false;
  uint16_t NextWrite = ( Buffer_Write + 1 ) % BUFFER_SIZE;
  if ( NextWrite == Buffer_Read ) {
    result = true;
  }
  return result;
}

bool Buffer_IsEmpty()
{
  return Buffer_Write == Buffer_Read;
}

int Buffer_Clear()
{
  Buffer_Read = 0;
  Buffer_Write = 0;
  return 0;
}
