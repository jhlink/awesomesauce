#ifndef BUFFER_H
#define BUFFER_H

#include "stdint.h"
#include "stdbool.h"
#include "stdlib.h"

#define BUFFER_SIZE ( 1000 + 1 )
#define BUFFER_ERROR_EMPTY 1;
#define BUFFER_ERROR_FULL 2;

int Buffer_Put(uint16_t Val);
int Buffer_Get(uint16_t* Val);
bool Buffer_IsFull();
bool Buffer_IsEmpty();
int Buffer_Clear();

#endif //BUFFER_H
