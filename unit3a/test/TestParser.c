#include "unity.h"
#include "Parser.h"
#include "MK20DX256.h"

extern PARSER_STATE_T state;

//  Tests for building design: Internal State access
//    These are implementation specific tests
void helper_CheckEndStateGivenStartStateAndChar( PARSER_STATE_T start_state, char NewChar, PARSER_STATE_T end_state)
{
  state = start_state;
  TEST_ASSERT_NULL(Parser_AddChar( NewChar ) );
  TEST_ASSERT_EQUAL( end_state, state );
}

void insert_valid_packet_with_payload(char cmd, int data_len, const char* payload, const char* full)
{
  TEST_ASSERT_NULL( Parser_AddChar( '[' ) );
  TEST_ASSERT_NULL( Parser_AddChar( cmd ) );
  TEST_ASSERT_NULL( Parser_AddChar( (char) data_len + '0' ) );
  for ( int i = 0; i < strlen(payload); i++) {
    TEST_ASSERT_NULL( Parser_AddChar( payload[i] ) );
  }

  TEST_ASSERT_EQUAL_STRING( full, Parser_AddChar( ']' ) );
}

void insert_valid_minimal_packet(char cmd, const char* full)
{
  TEST_ASSERT_NULL( Parser_AddChar( '[' ) );
  TEST_ASSERT_NULL( Parser_AddChar( cmd ) );
  TEST_ASSERT_NULL( Parser_AddChar( '0' ) );
  TEST_ASSERT_EQUAL_STRING( full, Parser_AddChar( ']' ) );
}

void test_Parse_AddChar_should_StartLookingForCmdOnLeftBracket(void)
{
  state = PARSER_LOOKING_FOR_START;

  TEST_ASSERT_NULL(Parser_AddChar('[') );
  TEST_ASSERT_EQUAL( PARSER_LOOKING_FOR_CMD, state );
}

void test_Parse_AddChar_should_RejectNonBracketCharactersWhileSeekingStart(void)
{
  state = PARSER_LOOKING_FOR_START;

  TEST_ASSERT_NULL( Parser_AddChar('|') );
  TEST_ASSERT_EQUAL( PARSER_LOOKING_FOR_START, state );

  TEST_ASSERT_NULL( Parser_AddChar('9') );
  TEST_ASSERT_EQUAL( PARSER_LOOKING_FOR_START, state );

  TEST_ASSERT_NULL( Parser_AddChar('A') );
  TEST_ASSERT_EQUAL( PARSER_LOOKING_FOR_START, state );

  TEST_ASSERT_NULL( Parser_AddChar('z') );
  TEST_ASSERT_EQUAL( PARSER_LOOKING_FOR_START, state );
}

void test_Parse_AddChar_should_AcceptAValidCommandCharacter(void)
{
  state = PARSER_LOOKING_FOR_CMD;
  TEST_ASSERT_NULL( Parser_AddChar('A') );
  TEST_ASSERT_EQUAL(PARSER_LOOKING_FOR_LEN, state);

  state = PARSER_LOOKING_FOR_CMD;
  TEST_ASSERT_NULL( Parser_AddChar('Z') );
  TEST_ASSERT_EQUAL( PARSER_LOOKING_FOR_LEN, state);
}

void test_Parse_AddChar_should_RejectInvalidCommandCharacter(void)
{
  // @ is the last invalid value before reaching upper A
  state = PARSER_LOOKING_FOR_CMD;
  TEST_ASSERT_NULL( Parser_AddChar('@') );
  TEST_ASSERT_EQUAL(PARSER_LOOKING_FOR_START, state);

  // \ is the last invalid value before reaching [
  state = PARSER_LOOKING_FOR_CMD;
  TEST_ASSERT_NULL( Parser_AddChar('\\') );
  TEST_ASSERT_EQUAL( PARSER_LOOKING_FOR_START, state);

  //  Reject lower case letters
  state = PARSER_LOOKING_FOR_CMD;
  TEST_ASSERT_NULL( Parser_AddChar('a') );
  TEST_ASSERT_EQUAL( PARSER_LOOKING_FOR_START, state);
}

void test_Parse_AddChar_should_AcceptALengthOfZero(void)
{
  helper_CheckEndStateGivenStartStateAndChar(PARSER_LOOKING_FOR_LEN, '0', PARSER_LOOKING_FOR_END);
}

void test_Parse_AddChar_should_RejectInvalidLengths(void)
{
  helper_CheckEndStateGivenStartStateAndChar(PARSER_LOOKING_FOR_LEN, ':', PARSER_LOOKING_FOR_START);
}

//  Tests for validating design: Public Interface access
//    Deal with API and verifies all functionality
void test_Parse_AddChar_should_HandleValidPacketWithNoData(void)
{
  insert_valid_minimal_packet('A', "[A0]");
}

void test_Parse_AddChar_should_HandleBackToBackValidPackets(void)
{
  insert_valid_minimal_packet('A', "[A0]");
  insert_valid_minimal_packet('Z', "[Z0]");
  insert_valid_minimal_packet('C', "[C0]");
}

void test_Parse_AddChar_should_IgnoreBadStartCharacters(void)
{
  TEST_ASSERT_NULL( Parser_AddChar( '?' ) );
  insert_valid_minimal_packet('C', "[C0]");

  TEST_ASSERT_NULL( Parser_AddChar( 'a' ) );
  TEST_ASSERT_NULL( Parser_AddChar( ']' ) );
  insert_valid_minimal_packet('C', "[C0]");
}

void test_Parse_AddChar_should_IgnoreBadCommandCharacters(void)
{
  TEST_ASSERT_NULL( Parser_AddChar( '[' ) );
  TEST_ASSERT_NULL( Parser_AddChar( 'a' ) );
  insert_valid_minimal_packet('D', "[D0]");

  TEST_ASSERT_NULL( Parser_AddChar( '[' ) );
  TEST_ASSERT_NULL( Parser_AddChar( 'a' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '0' ) );
  TEST_ASSERT_NULL( Parser_AddChar( ']' ) );
  insert_valid_minimal_packet('C', "[C0]");
}

void test_Parse_AddChar_should_HandleValidPacketWithOneByte(void)
{
  insert_valid_packet_with_payload( 'A', 1, "00", "[A100]");
}

void test_Parse_AddChar_should_HandleValidPacketWithTwoBytes(void)
{
  insert_valid_packet_with_payload( 'A', 2, "0000", "[A20000]");
}

void test_Parse_AddChar_should_HandleValidPacketWithNineBytes(void)
{
  insert_valid_packet_with_payload( 'A', 9, "123456789ABCDEF123", "[A9123456789ABCDEF123]");
}

void test_Parse_AddChar_should_HandleBackToBackValidPacketWithVariableSizes(void)
{
  insert_valid_packet_with_payload( 'A', 9, "123456789ABCDEF123", "[A9123456789ABCDEF123]");
  insert_valid_packet_with_payload( 'B', 3, "123456", "[B3123456]");
  insert_valid_packet_with_payload( 'C', 6, "123456789ABC", "[C6123456789ABC]");
  insert_valid_packet_with_payload( 'D', 2, "0123", "[D20123]");
}

void test_Parse_AddChar_should_HandleBackToBackValidPacketsWithPayload(void)
{
  insert_valid_packet_with_payload( 'A', 9, "123456789ABCDEF123", "[A9123456789ABCDEF123]");
  insert_valid_packet_with_payload( 'Z', 9, "123456789ABCDEF123", "[Z9123456789ABCDEF123]");
  insert_valid_packet_with_payload( 'B', 9, "123456789ABCDEF123", "[B9123456789ABCDEF123]");
  insert_valid_packet_with_payload( 'D', 9, "123456789ABCDEF123", "[D9123456789ABCDEF123]");
}

void test_Parse_AddChar_should_IgnorePacket_when_DataExceedsPayloadLengthOfZero(void)
{
  TEST_ASSERT_NULL( Parser_AddChar( '[' ) );
  TEST_ASSERT_NULL( Parser_AddChar( 'F' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '0' ) );
  TEST_ASSERT_NULL( Parser_AddChar( 'A' ) );
}

void test_Parse_AddChar_should_IgnorePacket_when_DataExceedsPayloadLengthOfFour(void)
{
  TEST_ASSERT_NULL( Parser_AddChar( '[' ) );
  TEST_ASSERT_NULL( Parser_AddChar( 'G' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '4' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '1' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '2' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '3' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '4' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '7' ) );
  TEST_ASSERT_NULL( Parser_AddChar( ']' ) );
}

void test_Parse_AddChar_should_IgnorePacket_when_EndCharacterIsInvalid(void)
{
  TEST_ASSERT_NULL( Parser_AddChar( '[' ) );
  TEST_ASSERT_NULL( Parser_AddChar( 'G' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '4' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '1' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '2' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '3' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '4' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '!' ) );
}

void test_Parse_AddChar_should_IgnorePacket_when_ReceivedDataExceedsPacketLength(void)
{
  TEST_ASSERT_NULL( Parser_AddChar( '[' ) );
  TEST_ASSERT_NULL( Parser_AddChar( 'G' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '4' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '1' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '2' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '3' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '4' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '4' ) );
  TEST_ASSERT_NULL( Parser_AddChar( ']' ) );
}

void test_Parse_AddChar_should_IgnorePacket_when_ReceivedDataIsLessThanPacketLength(void)
{
  TEST_ASSERT_NULL( Parser_AddChar( '[' ) );
  TEST_ASSERT_NULL( Parser_AddChar( 'G' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '4' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '1' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '2' ) );
  TEST_ASSERT_NULL( Parser_AddChar( '3' ) );
  TEST_ASSERT_NULL( Parser_AddChar( ']' ) );
}


int main(void)
{
  UNITY_BEGIN();

  RUN_TEST(test_Parse_AddChar_should_StartLookingForCmdOnLeftBracket);
  RUN_TEST(test_Parse_AddChar_should_RejectNonBracketCharactersWhileSeekingStart);

  RUN_TEST(test_Parse_AddChar_should_AcceptAValidCommandCharacter);
  RUN_TEST(test_Parse_AddChar_should_RejectInvalidCommandCharacter);

  RUN_TEST(test_Parse_AddChar_should_AcceptALengthOfZero);
  RUN_TEST(test_Parse_AddChar_should_RejectInvalidLengths);

  RUN_TEST(test_Parse_AddChar_should_HandleValidPacketWithNoData);
  RUN_TEST(test_Parse_AddChar_should_HandleBackToBackValidPackets);
  RUN_TEST(test_Parse_AddChar_should_IgnoreBadStartCharacters);
  RUN_TEST(test_Parse_AddChar_should_IgnoreBadCommandCharacters);

  //  Data
  RUN_TEST(test_Parse_AddChar_should_HandleValidPacketWithOneByte);
  RUN_TEST(test_Parse_AddChar_should_HandleValidPacketWithTwoBytes);
  RUN_TEST(test_Parse_AddChar_should_HandleValidPacketWithNineBytes);
  RUN_TEST(test_Parse_AddChar_should_HandleBackToBackValidPackets);
  RUN_TEST(test_Parse_AddChar_should_HandleBackToBackValidPacketWithVariableSizes);

  //  Error handling
  RUN_TEST(test_Parse_AddChar_should_IgnorePacket_when_DataExceedsPayloadLengthOfZero);
  RUN_TEST(test_Parse_AddChar_should_IgnorePacket_when_DataExceedsPayloadLengthOfFour);
  RUN_TEST(test_Parse_AddChar_should_IgnorePacket_when_EndCharacterIsInvalid);

  //  Invalid received data length with respect to packet length
  //    Unpaired nibbles
  RUN_TEST(test_Parse_AddChar_should_IgnorePacket_when_ReceivedDataIsLessThanPacketLength);
  RUN_TEST(test_Parse_AddChar_should_IgnorePacket_when_ReceivedDataExceedsPacketLength);
  return UNITY_END();
}

