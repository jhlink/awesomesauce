#ifndef PARSER_H
#define PARSER_H

#include "MK20DX256.h"
#include "stddef.h"
#include "stdint.h"
#include "string.h"

typedef enum PARSER_STATE_T {
  PARSER_LOOKING_FOR_START = 0,
  PARSER_LOOKING_FOR_CMD,
  PARSER_LOOKING_FOR_LEN,
  PARSER_LOOKING_FOR_DATA,
  PARSER_LOOKING_FOR_END
} PARSER_STATE_T;

char* Parser_AddChar(char NewChar);


#endif //PARSER_H
