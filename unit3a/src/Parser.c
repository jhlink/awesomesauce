#include "Parser.h"

#ifndef TEST
#define STATIC static
#else
#define STATIC
#endif

#define PARSER_BUFFER_LENGTH_MAX (23)

STATIC PARSER_STATE_T state = PARSER_LOOKING_FOR_START;

static char buffer[PARSER_BUFFER_LENGTH_MAX];
static uint32_t data_pos;

char* Parser_AddChar(char NewChar)
{
  if ( NewChar == '[' ) {
    state = PARSER_LOOKING_FOR_CMD;
    buffer[0] = NewChar;
    data_pos = 0;
  } else {
    switch ( state ) {
      case PARSER_LOOKING_FOR_CMD:
        if (( NewChar >= 'A' ) && ( NewChar <= 'Z' )) {
          state = PARSER_LOOKING_FOR_LEN;
          buffer[1] = NewChar;
        } else {
          state = PARSER_LOOKING_FOR_START;
        }
        break;

      case PARSER_LOOKING_FOR_LEN:
        if ( NewChar == '0' ) {
          state = PARSER_LOOKING_FOR_END;
          buffer[2] = NewChar;
        } else if ( NewChar > '0' && NewChar <= '9' )  {
          state = PARSER_LOOKING_FOR_DATA;
          buffer[2] = NewChar;
        } else {
          state = PARSER_LOOKING_FOR_START;
        }
        break;

      case PARSER_LOOKING_FOR_DATA:
        if ( data_pos < ( 2 * (buffer[2] - '0') ) ) {
          buffer[data_pos + 3] = NewChar;
          data_pos++;
          if ( data_pos == ( 2 * ( buffer[2] - '0' ) ) ) {
            state = PARSER_LOOKING_FOR_END;
          }
        }
        break;

      case PARSER_LOOKING_FOR_END:
        if ( NewChar == ']' ) {
          buffer[data_pos + 3] = NewChar;
          buffer[data_pos + 4] = 0; // Null terminate
          return buffer;
        }

        state = PARSER_LOOKING_FOR_START;
        break;

      default:
        break;
    }
  }

  return NULL;
}

