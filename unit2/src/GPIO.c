#include "GPIO.h"

static uint32_t ReverseMask;

int GPIO_SetPinAsOutput(uint8_t Pin)
{
  if (Pin >= 32) {
    return 1;
  }

  PORTC.PDDR |= BIT_TO_MASK(Pin);

  return GPIO_OK;
}

int GPIO_SetPinAsInput(uint8_t Pin)
{
  if (Pin >= 32) {
    return 1;
  }

  PORTC.PDDR &= ~(BIT_TO_MASK(Pin));

  return 0;
}

int GPIO_SetPin(uint8_t Pin)
{
  if ( Pin >= 32 ) {
    return 1;
  }
  uint32_t Mask = BIT_TO_MASK(Pin);

  if ( ReverseMask & Mask ) {
    PORTC.PCOR = Mask;
  } else {
    PORTC.PSOR = Mask;
  }

  //  Changing the data direction frequently can lead to
  //    unwanted glitches.
  //PORTC.PDDR |= BIT_TO_MASK(Pin);
  //
  //  Apply change only if target bit differs
  if (!(PORTC.PDDR & Mask) ) {
    PORTC.PDDR |= Mask;
  }

  return 0;
}

int GPIO_ClearPin(uint8_t Pin)
{
  if ( Pin >= 32 ) {
    return 1;
  }

  uint32_t Mask = BIT_TO_MASK(Pin);

  if ( ReverseMask & Mask ) {
    PORTC.PSOR = Mask;
  } else {
    PORTC.PCOR = Mask;
  }

  //  If the corresponding PDDR bit set as input,
  //   force it to output.
  if (!(PORTC.PDDR & Mask) ) {
    PORTC.PDDR |= Mask;
  }

  return 0;
}

int GPIO_ReadPin(uint8_t Pin) 
{
  uint32_t PinReadResult = GPIO_ERROR;
  if ( Pin < 32 ) {
    uint32_t Mask = BIT_TO_MASK(Pin);
    PinReadResult = (PORTC.PDIR ^ ReverseMask) & Mask;
  }

  return PinReadResult;
}

int GPIO_ReadPort() 
{
  return PORTC.PDIR ^ ReverseMask; 
} 

int GPIO_Init(void) 
{
  PORTC.PDDR = 0x1012A000;
  PORTC.PSOR = 0x10102000;
  PORTC.PCOR = 0x00028000;
  ReverseMask = 0x00000022;

  return GPIO_OK;
}
