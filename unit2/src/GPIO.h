#ifndef GPIO_H
#define GPIO_H

#include <stdint.h>
#include "MK20DX256.h"

int GPIO_SetPinAsOutput(uint8_t Pin);
int GPIO_SetPinAsInput(uint8_t Pin);
int GPIO_SetPin(uint8_t Pin);
int GPIO_ClearPin(uint8_t Pin);
int GPIO_ReadPin(uint8_t Pin);
int GPIO_ReadPort(void);
int GPIO_Init(void);

#endif //GPIO_H
