#include "unity.h"
#include "Filter.h"
#include "stddef.h"
#include "MK20DX256.h"

void test_Filter_AddVal_should_OutputPrevVal_if_PreValAndNewValMatch(void)
{
  TEST_ASSERT_EQUAL_HEX16( 0, Filter_AddVal( 0, 0) );
  TEST_ASSERT_EQUAL_HEX16( 0xFFFF, Filter_AddVal( 0xFFFF, 0xFFFF ) );
  TEST_ASSERT_EQUAL_HEX16( 0x8000, Filter_AddVal( 0x8000, 0x8000 ) );
  TEST_ASSERT_EQUAL_HEX16( 0x7FFF, Filter_AddVal( 0x7FFF, 0x7FFF ) );
}

void test_Filter_AddVal_should_HandleSimpleMiddleRangeCalculations(void)
{
  TEST_ASSERT_EQUAL_HEX16( 775, Filter_AddVal( 1000, 100 ) );
  TEST_ASSERT_EQUAL_HEX16( 325, Filter_AddVal( 100, 1000 ) );
}

void test_Filter_AddVal_should_HandleUInt16MaxSpikes(void) {
  //  PrevVal=65535, NextVal=1000, Fn=49401.25
  //    Note UINT16_MAX is 65535 not 65536 because 0 start index
  TEST_ASSERT_EQUAL_HEX16( 0xC0F9, Filter_AddVal( UINT16_MAX, 1000 ) );

  //  PrevVal=1000, NextVal=65535, Fn=17384
  TEST_ASSERT_EQUAL_HEX16( 0x42ED, Filter_AddVal( 1000 , UINT16_MAX ) );
}

void test_Filter_AddVal_should_ReturnMaxUInt16_given_MaxUInt16Inputs(void) {
  TEST_ASSERT_EQUAL_HEX16( UINT16_MAX, Filter_AddVal( UINT16_MAX, UINT16_MAX ) );
}

void test_Filter_AddVal_should_HandleZeroInputCalculations(void) {
  TEST_ASSERT_EQUAL_HEX16( 25, Filter_AddVal( 0, 100 ) );
  TEST_ASSERT_EQUAL_HEX16( 75, Filter_AddVal( 100, 0 ) );
}

int main(void) {
    UNITY_BEGIN();
    RUN_TEST(test_Filter_AddVal_should_OutputPrevVal_if_PreValAndNewValMatch);
    RUN_TEST(test_Filter_AddVal_should_HandleSimpleMiddleRangeCalculations);
    RUN_TEST(test_Filter_AddVal_should_HandleUInt16MaxSpikes);
    RUN_TEST(test_Filter_AddVal_should_ReturnMaxUInt16_given_MaxUInt16Inputs);
    RUN_TEST(test_Filter_AddVal_should_HandleZeroInputCalculations);
    return UNITY_END();
}
