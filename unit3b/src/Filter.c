#include "Filter.h"
#include "MK20DX256.h"

uint16_t Filter_AddVal(uint16_t PrevVal, uint16_t NewVal) 
{
  uint32_t product = ( (uint32_t) PrevVal ) * 3;
  return (uint16_t) ( ( product + NewVal ) / 4 );
}
